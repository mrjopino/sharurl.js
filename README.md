sharurl.js
==========

Track the number of times your page has been shared on several social networks

hey here demo: [http://mrjopino.github.io/sharurl.js](http://mrjopino.github.io/sharurl.js/)

```js
// sharurl.js
_sharurl = "your url"; // example
```

```html
<!-- example to implement -->
<span data-sharurl="twitter">0</span>
<span data-sharurl="facebook">0</span>
<span data-sharurl="linkedin">0</span>
<span data-sharurl="bufferapp">0</span>
```
-------------

Copyright, 2014 by [José Pino](http://twitter.com/mrjopino)

-------------
